#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

import sys
import zmq

#if __name__ == "__main__":
#	print("Not implemented.")	

# Set up the socket
context = zmq.Context()
socket = context.socket(zmq.SUB)

print("Initializing")
# Get the port number from the arguments, otherwise default is 9950
portnum = sys.argv[1] if len(sys.argv) > 1 else "9950"
# Connect to the port number on localhost
socket.connect("tcp://localhost:%s" %portnum)
print ("Connected to localhost on port %s" %portnum)

# Set the default socket option to the message
socket.setsockopt_string(zmq.SUBSCRIBE,'')
print ("Socket option set")

# Receive the message

i=0
string = 0
#while 1==1:
  # Loop until a message is received
while (i<10 and string == 0):
    print ("Starting Loop")
    #Receive the message
    messagerec = socket.recv()
    print ("Message received:")
    #Decode message
    messagerec = str(messagerec.decode('utf-8'))
    i+=1
    print (i)

  # Print the message
    print ("%s" %messagerec)

 
