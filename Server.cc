#include <csignal>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <zmp.hpp>

using namespace std;


void mySignalHandler (int sig){
    cout<< "Interrupt signal ("<< sig << ")recieved.\n";

    //close socket
    exit(sig);
}


int main(){
    signal (SIGINT, mySignalHandler);

    while(1){
        cout<<"Going to sleep..."<<endl;
        sleep(1);
    }
    return 0;
}
